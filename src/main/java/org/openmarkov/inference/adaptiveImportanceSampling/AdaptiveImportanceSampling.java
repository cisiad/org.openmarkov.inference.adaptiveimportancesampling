/*
 * Copyright 2012 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */
package org.openmarkov.inference.adaptiveImportanceSampling;

import java.util.HashMap;
import java.util.List;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.Potential;
import org.openmarkov.core.model.network.potential.TablePotential;

public class AdaptiveImportanceSampling extends InferenceAlgorithm
{
    public AdaptiveImportanceSampling (ProbNet probNet)
        throws NotEvaluableNetworkException
    {
        super (probNet);
    }

    @Override
    public boolean isEvaluable (ProbNet probNet)
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public TablePotential getGlobalUtility ()
        throws IncompatibleEvidenceException,
        UnexpectedInferenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public HashMap<Variable, TablePotential> getProbsAndUtilities ()
        throws IncompatibleEvidenceException,
        UnexpectedInferenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public HashMap<Variable, TablePotential> getProbsAndUtilities (List<Variable> variablesOfInterest)
        throws IncompatibleEvidenceException,
        UnexpectedInferenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TablePotential getJointProbability (List<Variable> variables)
        throws IncompatibleEvidenceException,
        UnexpectedInferenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Potential getOptimizedPolicy (Variable decisionVariable)
        throws IncompatibleEvidenceException,
        UnexpectedInferenceException
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Potential getExpectedUtilities (Variable decisionVariable)
        throws IncompatibleEvidenceException,
        UnexpectedInferenceException
    {
        // TODO Auto-generated method stub
        return null;
    }
}
