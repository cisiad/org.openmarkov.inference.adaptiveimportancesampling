package org.openmarkov.inference.adaptiveImportanceSampling;

import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.inference.InferenceAlgorithmTests;
import org.openmarkov.core.model.network.ProbNet;

public class AdaptiveImportanceSamplingTests extends InferenceAlgorithmTests
{
    @Override
    public InferenceAlgorithm buildInferenceAlgorithm (ProbNet probNet)
        throws NotEvaluableNetworkException
    {
        return new AdaptiveImportanceSampling (probNet);
    }
}
